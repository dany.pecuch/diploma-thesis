from flask import Flask, request, jsonify
import pandas as pd
from sqlalchemy import create_engine, text
from sqlalchemy.orm import sessionmaker
import requests
from io import StringIO
import logging
from datetime import datetime
from dotenv import load_dotenv
import os


load_dotenv()

app = Flask(__name__)
app.logger.setLevel(logging.INFO)  # Use INFO level for production

db_user = os.getenv('DB_USER')
db_password = os.getenv('DB_PASSWORD')
db_host = os.getenv('DB_HOST')
db_port = os.getenv('DB_PORT')
db_name = os.getenv('DB_NAME')
database_url = f'postgresql://{db_user}:{db_password}@{db_host}:{db_port}/{db_name}'
database_engine = create_engine(database_url)
Session = sessionmaker(bind=database_engine)

errors = {
    "url_empty": "URL cannot be empty!",
    "table_name_empty": "Table name cannot be empty!",
    "table_name_present": "Table name already present. Please choose a different name.",
    "download_csv_failed": "Failed to download CSV.",
    "load_csv_failed": "Failed to load CSV into database.",
    "fetch_records_failed": "Failed to fetch records.",
    "upload_csv_failed": "Failed to upload CSV.",
    "delete_table_failed": "Failed to delete table.",
    "reload_csv_failed": "Failed to reload CSV.",
    "invalid_file_extension": "Invalid file extension, expected CSV.",
    "fetch_records_failed": "Could not fetch records."
}

success_messages = {
    "csv_loaded": "CSV loaded into database successfully.",
    "table_deleted": "Table and its records deleted successfully.",
    "csv_reloaded": "CSV reloaded successfully, URL updated."
}

def table_exists(table_name):
    query = text("""
        SELECT EXISTS (
            SELECT FROM information_schema.tables 
            WHERE table_schema = 'public' 
            AND table_name = :table_name
        );
    """)
    with Session() as session:
        return session.execute(query, {'table_name': table_name}).scalar()

@app.route("/api/import_csv", methods=["POST"])
def import_csv():
    json_data = request.get_json()
    csv_url = json_data.get('csv_url', '')
    table_name = json_data.get('table_name', '')
    delimiter = json_data.get('delimiter', ';')

    if not csv_url:
        return jsonify({"error": errors["url_empty"]}), 400
    if not table_name:
        return jsonify({"error": errors["table_name_empty"]}), 400
    if table_exists(table_name):
        return jsonify({"error": errors["table_name_present"]}), 409
    if not delimiter:
        delimiter = ';'
    
    filename = csv_url.split('/')[-1].lower()
    if not filename.endswith('.csv'):
        return jsonify({"error": errors["invalid_file_extension"]}), 400
        
    df_to_sql_success = False

    try:
        response = requests.get(csv_url)
        response.raise_for_status()
        df = pd.read_csv(StringIO(response.text), delimiter=delimiter)

        with Session.begin() as session:
            df.to_sql(table_name, database_engine, index=False, if_exists='replace')
            df_to_sql_success = True
            if df_to_sql_success:
                session.execute(
                    text("INSERT INTO csv_uploads (csv_url, table_name, date_saved, data_delimiter) VALUES (:csv_url, :table_name, :date_saved, :data_delimiter)"),
                    {'csv_url': csv_url, 'table_name': table_name, 'date_saved': datetime.now(), 'data_delimiter': delimiter}
                )

        return jsonify({"message": success_messages["csv_loaded"]}), 200
    except requests.RequestException as e:
        return jsonify({"error": errors["download_csv_failed"], "message": str(e)}), 400
    except Exception as e:
        return jsonify({"error": errors["load_csv_failed"], "message": str(e)}), 500

@app.route("/api/get_uploaded_data", methods=["GET"])
def get_uploaded_data():
    try:
        with Session() as session:
            result = session.execute(text("SELECT csv_url, table_name, date_saved, data_delimiter FROM csv_uploads"))
            uploads = [
                {
                    "csv_url": row.csv_url, 
                    "table_name": row.table_name, 
                    "date_saved": row.date_saved.isoformat() if row.date_saved else None,
                    "data_delimiter": row.data_delimiter
                }
                for row in result
            ]
        return jsonify(uploads), 200
    except Exception as e:
        return jsonify({"error": errors["fetch_records_failed"], "message": str(e)}), 500

@app.route("/api/reload_csv_data", methods=["POST"])
def reload_csv_data():
    json_data = request.get_json()
    csv_url = json_data.get('csv_url', '')
    table_name = json_data.get('table_name', '')
    delimiter = json_data.get('delimiter', ';')

    if not delimiter:
        delimiter = ';'
        
    filename = csv_url.split('/')[-1].lower()
    if not filename.endswith('.csv'):
        return jsonify({"error": errors["invalid_file_extension"]}), 400

    try:
        response = requests.get(csv_url)
        response.raise_for_status()

        df = pd.read_csv(StringIO(response.text), delimiter=delimiter)
        
        df_to_sql_success = False

        with Session() as session:
            with session.begin():
                df.to_sql(table_name, session.bind, index=False, if_exists='replace')
                df_to_sql_success = True
                
                if df_to_sql_success:
                    session.execute(
                        text("UPDATE csv_uploads SET csv_url = :csv_url, date_saved = :current_date, data_delimiter = :delimiter WHERE table_name = :table_name"),
                        {'csv_url': csv_url, 'current_date': datetime.now(), 'table_name': table_name, 'delimiter': delimiter}
                    )

                    updated_info = session.execute(
                        text("SELECT csv_url, date_saved, data_delimiter FROM csv_uploads WHERE table_name = :table_name"),
                        {'table_name': table_name}
                    ).fetchone()
                else:
                    return jsonify({"error": errors["reload_csv_failed"]}), 500
        return jsonify({
            "message": success_messages["csv_reloaded"],
            "csv_url": updated_info.csv_url,
            "date_saved": updated_info.date_saved.isoformat(),
            'data_delimiter': updated_info.data_delimiter
        }), 200

    except Exception as e:
        return jsonify({
            "error": errors["reload_csv_failed"],
            "message": str(e)
        }), 500

@app.route("/api/delete_csv", methods=["DELETE"])
def delete_csv():
    json_data = request.get_json()

    table_name = json_data.get('table_name')
    if not table_name:
        return jsonify({"error": errors["table_name_empty"]}), 400
    
    table_name_quoted = f'"{table_name}"'

    try:
        with Session.begin() as session:
            session.execute(text(f"DROP TABLE IF EXISTS {table_name_quoted}"))
            session.execute(
                text("DELETE FROM csv_uploads WHERE table_name = :table_name"),
                {'table_name': table_name}
            )
        return jsonify({"message": success_messages["table_deleted"]}), 200
    except Exception as e:
        return jsonify({"error": errors["delete_table_failed"], "message": str(e)}), 500



if __name__ == "__main__":
    app.run(debug=False)
