import React from 'react';
import { Container, Navbar, Nav } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';

export default function NavigationBar() {
    return (
        <Navbar collapseOnSelect expand="lg" variant="dark" className="custom-navbar">
            <Container>
                <Navbar.Brand href="/home">Postgres Manager</Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="me-auto">
                        <LinkContainer to="/home">
                            <Nav.Link>Import Data</Nav.Link>
                        </LinkContainer>
                        <LinkContainer to="/manage-data">
                            <Nav.Link>Manage Data</Nav.Link>
                        </LinkContainer>
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}
