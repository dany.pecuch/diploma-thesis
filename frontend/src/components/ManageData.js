import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrashAlt, faEdit, faSpinner } from '@fortawesome/free-solid-svg-icons';
import { format } from 'date-fns';
import 'react-toastify/dist/ReactToastify.css';
import '../design/ManageData.css'
import Modal from 'react-modal';

Modal.setAppElement('#root');

export default function ManageData() {
  const [uploads, setUploads] = useState([]);
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(null);
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const [modalLoading, setModalLoading] = useState(false);
  const [currentEdit, setCurrentEdit] = useState({
    index: -1,
    csv_url: '',
    delimiter: ''
  });

  useEffect(() => {
    async function fetchUploads() {
      try {
        const response = await axios.get('/api/get_uploaded_data');
        setUploads(response.data);
        setLoading(false);
      } catch (err) {
        setError(err.response.data.error);
        setLoading(false);
        toast.error(`Error fetching data: ${err.message}`);
      }
    }

    fetchUploads();
  }, []);

  const handleDelete = async (index) => {
    const table_name = uploads[index].table_name;
    if (window.confirm(`Are you sure you want to delete the table "${table_name}"? This action cannot be undone.`)) {
        try {
            const response = await axios.delete('/api/delete_csv', {
                data: { table_name }
            });
            if (response.status === 200) {
                toast.success(response.data.message);
                setUploads(prev => prev.filter((_, i) => i !== index));
            }
        } catch (error) {
            toast.error(`Failed to delete CSV: ${error.response ? error.response.data.error : error.message}`);
        }
    }
};


  const openModal = (index) => {
    setCurrentEdit({ index: index, csv_url: uploads[index].csv_url, delimiter: uploads[index].data_delimiter });
    setModalIsOpen(true);
  };

  const closeModal = () => {
    setModalIsOpen(false);
    setModalLoading(false);
  };

  const confirmEdit = async () => {
    const { index, csv_url, delimiter } = currentEdit;
    setModalLoading(true);
    try {
      const response = await axios.post('/api/reload_csv_data', {
        csv_url: csv_url,
        table_name: uploads[index].table_name,
        delimiter: delimiter
      });
      if (response.data) {
        toast.success('CSV updated successfully!');
        setUploads(prev => {
          const newUploads = [...prev];
          newUploads[index] = { ...newUploads[index], csv_url: response.data.csv_url, date_saved: response.data.date_saved, data_delimiter: response.data.data_delimiter };
          return newUploads;
        });
      }
      closeModal();
    } catch (error) {
      toast.error(`Failed to update CSV: ${error.response ? error.response.data.error : error.message}`);
    } finally {
      setModalLoading(false);
    }
  };

  if (loading) return <div className="container"><h2>Loading...</h2></div>;
  if (error) return <div className="container"><h2>Error: {error}</h2></div>;

  return (
    <div className="container mt-5">
      <ToastContainer />
      <h1 className="mb-4">Manage Data</h1>
      <table className="table table-hover">
        <thead className="table-light">
          <tr>
            <th>#</th>
            <th>Table Name</th>
            <th>CSV URL</th>
            <th>Delimiter</th>
            <th>Last Updated</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {uploads.map((upload, index) => (
            <tr key={index}>
              <td>{index + 1}</td>
              <td>{upload.table_name}</td>
              <td className="url-cell">
                <a href={upload.csv_url} target="_blank" rel="noopener noreferrer" title={upload.csv_url}>
                  {upload.csv_url}
                </a>
              </td>
              <td>{upload.data_delimiter}</td>
              <td>{format(new Date(upload.date_saved), 'dd.MM.yyyy')}</td>
              <td>
                <div className="action-buttons">
                  <button className="btn btn-info btn-sm" onClick={() => openModal(index)}>
                    <FontAwesomeIcon icon={faEdit} />
                  </button>
                  <button className="btn btn-danger btn-sm" onClick={() => handleDelete(index)}>
                    <FontAwesomeIcon icon={faTrashAlt} />
                  </button>
                </div>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      <Modal
        isOpen={modalIsOpen}
        onRequestClose={closeModal}
        className="Modal"
        overlayClassName="Overlay"
      >
        <div>
          <h2>Edit CSV Data</h2>
          <label>
            CSV URL:
            <input
              type="text"
              value={currentEdit.csv_url}
              onChange={e => setCurrentEdit({ ...currentEdit, csv_url: e.target.value })}
              className="modal-input"
            />
          </label>
          <label>
            Delimiter:
            <input
              type="text"
              value={currentEdit.delimiter}
              onChange={e => setCurrentEdit({ ...currentEdit, delimiter: e.target.value })}
              placeholder="Default delimiter is ';'"
              className="modal-input"
            />
          </label>
          <div>
            <button onClick={confirmEdit} className="modal-button" disabled={modalLoading}>
              {modalLoading ? <FontAwesomeIcon icon={faSpinner} spin /> : 'Confirm'}
            </button>
            <button onClick={closeModal} className="modal-button cancel">Cancel</button>
          </div>
        </div>
      </Modal>
    </div>
  );
  
}
