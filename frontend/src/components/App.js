import React from 'react';
import { BrowserRouter as Router, Routes, Route, Navigate } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../design/App.css';
import Navbar from './NavigationBar';
import ImportData from './ImportData';
import ManageData from './ManageData';

function App() {
  return (
    <Router>
      <div>
        <Navbar />
        <Routes>
          {/* Redirect from root to /home */}
          <Route path="/" element={<Navigate replace to="/home" />} />
          {/* Home Page */}
          <Route path="/home" element={<ImportData />} />
          {/* Manage Data Page */}
          <Route path="/manage-data" element={<ManageData />} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;
