import React, { useState } from 'react';
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export default function ImportData() {
    const [csvUrl, setCsvUrl] = useState('');
    const [tableName, setTableName] = useState('');
    const [delimiter, setDelimiter] = useState('');
    const [isValidName, setIsValidName] = useState(true);
    const [errorMessage, setErrorMessage] = useState('');
    const [loading, setLoading] = useState(false);

    const validateTableName = (name) => {
        if (name === "") {
            setErrorMessage('');
            setIsValidName(true);
            return true;
        }

        const regex = /^[a-zA-Z_][a-zA-Z0-9_]*$/;
        if (name.length > 63) {
            setErrorMessage('Table name must be under 64 characters.');
            setIsValidName(false);
            return false;
        } else if (!regex.test(name)) {
            setErrorMessage('Invalid table name. Must start with a letter or underscore, followed by letters, digits, or underscores.');
            setIsValidName(false);
            return false;
        } else {
            setErrorMessage('');
            setIsValidName(true);
            return true;
        }
    };

    const preprocessUrl = (url) => {
        if (!url.startsWith('http://') && !url.startsWith('https://')) {
            return `http://${url}`;
        }
        return url;
    };

    const handleSubmit = async (event) => {
        event.preventDefault();
        if (tableName && !validateTableName(tableName)) {
            toast.error('Invalid table name. Please correct the errors and try again.');
            return;
        }
        setLoading(true); // Start loading
        const formattedUrl = preprocessUrl(csvUrl);
        try {
            const response = await axios.post('/api/import_csv', { csv_url: formattedUrl, table_name: tableName, delimiter: delimiter });
            console.log(response.data);
            toast.success('CSV loaded successfully!');
        } catch (error) {
            if (error.response && error.response.data.error) {
                toast.error(`Failed to load CSV: ${error.response.data.error}`);
            } else {
                toast.error(`Error: ${error.message}`);
            }
        } finally {
            setLoading(false);
        }
    };

    return (
        <div className="container mt-5">
            <ToastContainer />
            <h1 className="mb-4">Import CSV Data</h1>
            <form onSubmit={handleSubmit}>
                <div className="mb-3">
                    <label htmlFor="csvUrl" className="form-label">CSV file URL:</label>
                    <input type="text" className="form-control" id="csvUrl" value={csvUrl} onChange={e => setCsvUrl(e.target.value)} />
                </div>
                <div className="mb-3">
                    <label htmlFor="tableName" className="form-label">Table Name:</label>
                    <input type="text" className={`form-control ${!isValidName && tableName ? 'is-invalid' : ''}`} id="tableName"
                           value={tableName}
                           onChange={e => {
                               setTableName(e.target.value);
                               validateTableName(e.target.value);
                           }} />
                    {!isValidName && tableName && <div className="invalid-feedback">{errorMessage}</div>}
                </div>
                <div className="mb-3">
                    <label htmlFor="delimiter" className="form-label">Delimiter:</label>
                    <input type="text" className="form-control" id="delimiter" value={delimiter}
                           onChange={e => setDelimiter(e.target.value)} placeholder="Default delimiter is ';'" />
                </div>
                <button type="submit" className="btn btn-primary btn-lg" disabled={loading}>
                    {loading ? <i className="fas fa-spinner fa-spin"></i> : <i className="fas fa-cloud-upload-alt"></i>} Upload CSV
                </button>
            </form>
        </div>
    );
}
