CREATE TABLE IF NOT EXISTS csv_uploads (
    csv_url VARCHAR(255),
    table_name VARCHAR(255),
    date_saved TIMESTAMP WITHOUT TIME ZONE,
    data_delimiter VARCHAR(255)
);
